---
title: E-Sports Trial Act 2017
introducer: showstealer1829
number: 12
year: 2017
assent: 2017-12-01
urls:
- https://docs.google.com/document/d/188BXZNSiw4FqKVUOnn_vaE3vu_cmO6ehX22rtuXbuM8/edit
- https://www.reddit.com/r/AustraliaSim/comments/7ghdcv/presentation_of_esports_trial_bill_2017_for_royal/
---
