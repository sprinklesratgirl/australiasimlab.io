---
title: AustraliaSim Parliamentary Consultative Body Act 2017
#introducer: Unknown
number: 5
year: 2017
assent: 2017-07-05
urls:
- "https://www.reddit.com/r/AustraliaSim/comments/6lbm0a/royal_assent_for_the_australiasim_parliamentary/"
---
