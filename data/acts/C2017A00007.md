---
title: Code of Conduct Amendment Act 2017
introducer: mtmdog
number: 7
year: 2017
assent: 2017-07-28
urls:
- "https://www.reddit.com/r/AustraliaSim/comments/6q2qyy/declaration_of_royal_assent_to_the_appropriation/"
amends:
- C2017A00003
---
