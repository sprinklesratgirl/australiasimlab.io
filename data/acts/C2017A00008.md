---
title: Appropriation (Parliamentary Departments) Act (No. 1) 2017–2018
#introducer: Unknown
number: 8
year: 2017
assent: 2017-07-28
urls:
- "https://www.reddit.com/r/AustraliaSim/comments/6q2qyy/declaration_of_royal_assent_to_the_appropriation/"
---
