---
title: Removing Discrimination from Schools Act 2019
introducer: dyljam
assent: 2019-01-18
urls:
- https://docs.google.com/document/d/1kUPvH2WGrSgpf_oOnNg5o2yE2gNB84ByHFr2tQkqTOc/edit
- https://docs.google.com/document/d/1pavT-6vmSNFg-W47RH6k0GkkDzrpQp8gVHuqC1BF4-o/edit
amends:
- Sex Discrimination Act 1984
- Fair Work Act 2009
---
