---
title: Australian Medical Safety and Protection Act 2017
introducer: Kaarrien
number: 2
year: 2017
assent: 2017-04-20
urls:
- "https://www.reddit.com/r/AustraliaSim/comments/637lh1/bill_4_first_reading_australian_medical_safety/"
- "https://www.reddit.com/r/AustraliaSim/comments/66homs/royal_assent_of_approved_legislation/"
amends:
- Social Services Legislation Amendment (No Jab, No Pay) Act 2015
---

A Bill for an Act to devolve federal control over certain areas of healthcare to local communities

The Parliament of Australia enacts:

* **Short Title**
 This bill may be cited as the Australian Medical Safety and Protection Act 2017, or AMSPA.

* **Commencement**
 The provisions in this law will commence immediately following royal assent

**Schedule**

**1. Repeal of 'No Jab, No Pay'**

a) This bill repeals 'Social Services Legislation Amendment (No Jab, No Pay) Bill 2015', which amends 'A New Tax System (Family Assistance) Act 1999'
