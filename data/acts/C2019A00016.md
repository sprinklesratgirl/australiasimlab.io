---
title: Telecommunications Amendment (Guaranteeing Mobile Phone Service in Bushfire Zones) Act 2019
introducer: Youmaton
assent: 2019-05-02
urls:
- https://docs.google.com/document/d/1UBN_TbX9r9QPF4FoQruvSJu9Q7Ve4J4F39vjM2Wr10M/edit
- https://docs.google.com/document/d/1lOskxEZv-okYaUBEl03ABX_KMn6liRO23txUDM6tszI/edit
amends:
- Telecommunications Act 1997
---
